var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var app = express();
// var io = require('socket.io')(app);

var mongoDB = 'mongodb://127.0.0.1/mikrotik';
mongoose.connect(mongoDB);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Routes
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var serversRouter = require('./routes/servers');



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/servers', serversRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404);
    res.send({error: 404})
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.send({
      status: 'error',
      message: err.message
  });
});

// io.on('connection', function(socket) {
//     socket.on('join room', function(data) {
//         if (data.server_id)
//         {
//             socket.join("server " + data.server_id);
//             socket.emit("join room:success");
//         }
//     });
// });

module.exports = app;
