var Server = require('../models/Server');

module.exports = {
    index: function(req, res) {
        Server.find({}, function(err, list) {
            res.send(list);
        });
    },

    add: function(req, res) {
        Server.create({
            host: req.body.host,
            user: req.body.user,
            pass: req.body.pass
        }).then(function(err, server) {
            res.send(server ? server : err);
        })
    }
};
