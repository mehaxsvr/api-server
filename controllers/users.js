var User = require('../models/user');

exports.index = function(req, res) {
    User.find({}, function(err, list) {
        res.send(list);
    });
};

exports.add = function(req, res) {
    User.create({
        name: req.body.name,
        ip: req.body.ip
    }).then(function(user) {
        res.send(user);
    })
};