var express = require('express');
var router = express.Router();

var user_controller = require('../controllers/users');

router.get('/', user_controller.index);
router.post('/', user_controller.add);

module.exports = router;
