var express = require('express');
var router = express.Router();

var servers_controller = require('../controllers/servers');

router.get('/', servers_controller.index);
router.post('/', servers_controller.add);

module.exports = router;