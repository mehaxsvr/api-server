var mongoose = require('mongoose');

var ServerSchema = new mongoose.Schema({
    host: String,
    user: String,
    pass: String
});

var Server = mongoose.model('server', ServerSchema);
module.exports = Server;