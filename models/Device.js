var mongoose = require('mongoose');

var DeviceSchema = new mongoose.Schema({
    user_id: Int,
    name: String,
    ip: String
});
