var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    name: String,
    ip: String,
    ranges: Object
});
var User = mongoose.model('user', UserSchema);
module.exports = User;
